/*
  Biblioteka LiquidCrystal 
 
 Demonstruje u?ycie wy?wietlacza LCD 16x2. The LiquidCrystal
 biblioteka dzia?a ze wszystkimi wy?wietlaczami LCD zgodnymi z
 Sterownik Hitachi HD44780. Jest ich wielu i ty
 zwykle mo?na je przekaza? za pomoc? interfejsu 16-stykowego.
 
  Obw�d:
 * LCD RS pin to digital pin 12 (pin4 na LCD)
 * LCD W??cz pin do cyfrowego pinu 11 (pin6 na LCD)
 * Pin D4 LCD do cyfrowego pinu 5 (pin 11 na LCD)
 * Pin D5 LCD do cyfrowego z??cza 4 (pin12 na LCD)
 * Pin D6 LCD do cyfrowego pin 3 (pin 13 na LCD)
 * Pin LCD D7 do cyfrowego pinu 2 (pin 14 na LCD)
 * LCD R / W pin do GDN (pin5 na LCD)
 * Rezystor 10K: (potencjometr)
 * na + 5V i GDM (pin 1 i 16 na LCD)
 * Wiper do LCD VO pin (pin 3) (pin 3 na LCD do wyjscia z potencjometra)

 */

// do??czenie biblioteki Liquid Cristal
#include <LiquidCrystal.h>

// deklarowanie przypisanie pin�w wy?wietlacza
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  
  // ustawienie wielkosci wy?wietlacza: 
  lcd.begin(16, 2);
  // wyswietlanie wiadomosci na wyswietlaczu.
  lcd.print("Miernik Swiatla");
	// ustawienie kursora 0,1
  lcd.setCursor(0, 1);
  lcd.print("JASNOSC");
  lcd.setCursor (14, 1);
  lcd.print("%");
}

//glowna petla
void loop() {
  // odczytanie pinu analogowego  
  int sensorValue = analogRead(A0);
	// konwertujemy odczyana warto?? (0 - 1023)(0 -5v)
float swiatlo = sensorValue * (100.0 / 1023.0);
	//ustawiamy kursor
  lcd.setCursor(8, 1);
  // wyswietlamu warto??
  lcd.print(swiatlo);

delay (500);
  
}
