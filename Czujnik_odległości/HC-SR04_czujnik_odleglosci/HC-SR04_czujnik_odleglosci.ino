#define trigPin 12
#define echoPin 11
#define ledPin 13

/*
  Czujnik odległości podłączenie:
  Vcc - zasilanie +5V
  echoPin - pin 11
  trigPin - pin 12
  GND - masa 

  Dioda LED anoda - pin 13 przez rezystor ok. 200-300 Ohm
  Dioda LED katoda - masa GND

  Buzer pin1 - pin A5 bez rezystora
  Buzer pin2 - masa GND
 */

void setup() {
  // Inicjacja komunikacji serial do komputera
  Serial.begin(115200);
  Serial.println("Inicjacja komunikacji");
  // Inicjacja wejść i wyjść Arduino
  pinMode(ledPin, OUTPUT);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(A5, OUTPUT);
}

// Funkcja mierzaca i zwracająca odleglość w cm
// Do funkcji nie trzeba dostarczać niczego. Funkcja zwraca zmierzoną odległość jako INT.
int zmierzOdleglosc(){
  int dystans = 0;
  // Wyzwolenie pomiaru przez podanie sygnału HIGH (+5V) na złącze trigPin czujnika odległości
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  // Odczyt wyniku pomiaru z pinu echoPin czujnika
  // Czujnik na pin echoPin zwraca impuls (stan HIGH +5V) proporcjonalny do zmierzonej odległości. My mierzymy długość trwania tego impulsu w mikrosekundach.
  // Wyliczanie odległości - wzór z dokumentacji czujnika: cm = długość_impulsu / 58
  dystans = pulseIn(echoPin, HIGH) / 58;
  return dystans;
}

// Funkcja odtwarzająca dźwięk na buzzerze podłączonym do A5
// Do funkcji trzeba dostarczyć długość jako INT. Funkcja nic nie zwraca.
void sygnal(int czas){
  // Odtwarzaj dźwięk o częstotliwości 3500Hz na pinie A5 
  tone(A5, 3500);
  delay(czas*2);
  noTone(A5);
  delay(czas*2);
}

void loop() {
  // Wywołanie funkcji która mierzy odległość, wynik zapisywany jest do zmiennej odległość
  int odleglosc = zmierzOdleglosc();
  Serial.print(odleglosc);
  Serial.println(" cm");
  // Wywołanie funkcji odtwarzającej dźwięk zależny od odległości. Do funkcji przekazujemy odległość.
  sygnal(odleglosc);
  // Zapalanie alarmu (dioda LED) jeśli odległość mniejsza niż 50cm
  if (odleglosc < 50){
    digitalWrite(ledPin, HIGH);
  }else{
    digitalWrite(ledPin, LOW);
  }
  delay(200);
}
